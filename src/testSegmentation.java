import jvntextpro.*;
public class testSegmentation {

	public static void main(String[] args) {
		JVnTextPro textPro = new JVnTextPro();
		textPro.initPosTagger("models/jvnpostag/maxent");
		textPro.initSegmenter("models/jvnsegmenter");
		String text1 = "1 Nước Cam Tươi 100% + 2 Soda Táo Xanh và 8 cốc Hazelnut Đá Xay + 1 cốc Toffee Hạnh Nhân Nóng";
		String text2 = "1 nước caf phee 100% + 2 soda táo xanh và 8 cốc hazelnut ddá xay + 1 cốc toffee hạnh nhân nóng";
		String word_seg1 = textPro.wordSegment(text1);
		String word_seg2 = textPro.wordSegment(text2);
		String result1 = textPro.posTagging(word_seg1);
		String result2 = textPro.posTagging(word_seg2);
		System.out.println(result1);
		System.out.println(result2);

	}

}
