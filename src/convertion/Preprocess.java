package convertion;

import java.util.ArrayList;

public class Preprocess {
	public static String[] splitWord(String string,String delimiters) {
		ArrayList<String>  result = new ArrayList<String>();
//		 định nghĩa dấu 
		char hoi = "ổ".charAt(1);
		char sac = "ớ".charAt(1);
		char huyen = "ề".charAt(1);
		char nga = "ã".charAt(1);
		char nang = "ộ".charAt(1);
		String[] after_split_by_delimiters  = string.split(delimiters);
		for (String sub_string : after_split_by_delimiters) {
			String tem = "";
//			boolean is_added = false;
			for (char ch: sub_string.toCharArray()) {
//				System.out.println(ch + "---"+ Character.getNumericValue(ch));
//				Character.is
				if(Character.isLetterOrDigit(ch) || ch == hoi || ch == huyen || ch == nga || ch== nang || ch == sac) tem+= ch;
				else{
					if(!tem.equals("")) result.add(tem);
					result.add(new String() + ch);
					tem = "";
				}
			}
			if(!tem.equals("")) result.add(tem);
			
		}
		
		String[] final_result = new String[result.size()] ;
		return result.toArray(final_result);
	}
	
	public static void main(String[] args){
		String str = "chiều nay nhé_hihi";
		String str2 = "chiều nay buổi chiều";
		String str3 = "buổi chiều";
		String[] teStrings = Preprocess.splitWord("ở hihi", " |\\_");
		for (String string : teStrings) {
			System.out.println(string );
		}
	}

}
