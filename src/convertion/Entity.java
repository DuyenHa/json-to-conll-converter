package convertion;

import java.util.ArrayList;
import java.util.Arrays;

public class Entity {
	private String entity;
	private String value;
	private int start;
	private int end;
	private Entity[] subentities;
	
	public Entity () {
		
	}
	
	public Entity(String entity_, String value_ , int start_, int end_ ){
		entity = entity_; 
		value = value_ ;
		start = start_;
		end = end_;
	}
	
	public void showEntity(){
		System.out.println("entity : " + entity);
		System.out.println("value : "+ value);
		System.out.println("start : " + start);
		System.out.println("end : "+ end);
		if(subentities!= null)
		{
			System.out.println("subentities :");
			for (Entity entity : subentities) {
				entity.showEntity();
			}
			
		}
	}
	
	/**
	 * @return the entity
	 */
	public String getEntity() {
		return entity;
	}
	/**
	 * @param entity the entity to set
	 */
	public void setEntity(String entity) {
		this.entity = entity;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}
	/**
	 * @return the end
	 */
	public int getEnd() {
		return end;
	}
	/**
	 * @param end the end to set
	 */
	public void setEnd(int end) {
		this.end = end;
	}
	/**
	 * @return the subentities
	 */
	public Entity[] getSubentities() {
		return subentities;
	}
	/**
	 * @param subentities the subentities to set
	 */
	public void setSubentities(Entity[] subentities) {
		this.subentities = subentities;
	}
	
	public void addSubEntity ( Entity en){
		if(subentities == null){
			subentities = new Entity[1];
			subentities[0] = en;
		}
		else{
			Entity[] tem = subentities;
			subentities = new Entity [subentities.length + 1];
			int i;
			for (i = 0 ; i < tem.length; i ++) {
				subentities[i] = tem[i];
			}
			subentities[i] = en;
		}
//		ArrayList<Entity> tem;
//		if(subentities == null) {
//			 tem = new ArrayList<Entity>();
//		}
//		else tem = new ArrayList<Entity>(Arrays.asList(subentities));
//		tem.add(en);
//		subentities =  (Entity[]) tem.toArray();
	}
	
}
