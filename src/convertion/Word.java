package convertion;

import java.util.ArrayList;

public class Word {
	 String content;
	 String pos;
	 ArrayList<String> bio;
	 ArrayList<Integer> entity_id;
	
	public Word(String con_,ArrayList<String> bio_, ArrayList<Integer> entity_id_){
		content = con_;
		bio = bio_; 
		entity_id = entity_id_;
	}
	
//	Convert string to Word object : "1		M	B-product	B-sys_number	 O"
	public Word(String str){
		String[] results = str.split("\t");
		content = results[0];
		pos = results[1];
		bio = new ArrayList<String>();
		for(int i = 2 ; i < results.length;i++){
			bio.add(results[i]);
		}
	}
	
	public void showWord(){
		System.out.println(content +"\t"+ pos + "\t"+bio.toString() + "\t" + entity_id.toString());
	}
	
//	public String showWord_standar (int total_level){
//		String result= content +"\t"+ pos;
//		for(int i = 0 ; i < total_level ; i ++){
//			if(i < bio.size()) result = result + "\t"+bio.get(i);
//			else result += "\t O";
//		}
//		result = result +"\n";
//		System.out.println(result);
//		return result;
//	}
	
	public String showWord_standar (int total_level){
		String result= content +" "+ pos;
		for(int i = 0 ; i < total_level ; i ++){
			if(i < bio.size()) result = result + " "+bio.get(i);
			else result += " O";
		}
		result = result +"\n";
		System.out.println(result);
		return result;
	}
	
	public int getLevel(){
		return bio.size();
	}

}
