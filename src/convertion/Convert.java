package convertion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.sound.midi.Soundbank;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jvntextpro.JVnTextPro;

public class Convert {
	
	public static void Json2Conll(ObjectMapper objectMapper, String data, String outfile) throws JsonParseException, JsonMappingException, IOException {
		File file = new File(data);
		JVnTextPro textPro = new JVnTextPro();
		textPro.initPosTagger("models/jvnpostag/maxent");
		textPro.initSegmenter("models/jvnsegmenter");
		Document myDocument = objectMapper.readValue(file, Document.class);
//		myDocument.showDocument();
		myDocument.getConllFormat(textPro, outfile);
//		objectMapper.writeValue(
//			    new FileOutputStream("data/convert/json_format.json"), myDocument.getData()[0]);
	}
	
	public static void Coll2Json(ObjectMapper objectMapper, String infile, String outfile) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(
        new FileInputStream(infile), "UTF-8"));
		String line;
		Document myDocument = new Document();
		ArrayList<Sentence> sentences  = new ArrayList<Sentence>();
		Sentence sen = new Sentence();
		String sen_text = "";
		String entity_content = "";
		
		while ((line = in.readLine()) != null){
			if(line.equals("")) {
				sentences.add(new Sentence(sen));
				sen = new Sentence();
			}
			else{
				sen.setWordsFromString(line);
//				System.out.println(line);
			}
			
		}
		
		for (Sentence sentence : sentences) {
			System.out.println(sentence.getText());
			sentence._setEntities();
//			System.out.println(sentence.max_level);
//			sentence.WordsFormatted();
		}
		
		objectMapper.writeValue(
	    new FileOutputStream(outfile),sentences.get(0) );
		
	}
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
		ObjectMapper objectMapper = new ObjectMapper();
		if(args[0].equals("toConll")) Json2Conll(objectMapper, args[1],args[2]);
		if(args[0].equals("toJson")) Coll2Json(objectMapper, args[1],args[2]);
//		Json2Conll(objectMapper, "data/convert/Trang_data_fixed.json","data/convert/Trang_data.txt");
//		Json2Conll(objectMapper, "data/convert/Xuan_data.json","data/convert/Xuan_data.txt");
//		Json2Conll(objectMapper, "data/convert/data.json","data/convert/out_data.txt");
//		Json2Conll(objectMapper,"data/convert/coffee_data.json");
//		file input có 2 dòng cuối trống
//		Coll2Json(objectMapper,"data/convert/bio_test","out.json");			
	}

}
