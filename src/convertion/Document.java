package convertion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import com.fasterxml.jackson.databind.ObjectMapper;

import jvntextpro.JVnTextPro;

public class Document {
	private Sentence[] data;

	/**
	 * @return the data
	 */
	public Sentence[] getData() {
		return data;
	}
	
	public void showDocument(){
		for (Sentence sentence : data) {
			sentence.showSentence();
		}
	}

	public void getConllFormat(JVnTextPro textPro,String outfile) throws IOException {
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
        new FileOutputStream(outfile), "UTF-8"));
		for (Sentence sen : data) {
			if(sen.getEntities() == null ) {
				System.out.println("Don't have data in sentence : " + sen.getText());
				continue;
			}
			sen.testGetWordsBIO();
			sen.setPOS(textPro);
			out.write(sen.WordsFormatted());
			out.newLine();
		}
		out.close();
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Sentence[] data) {
		this.data = data;
	}


}
