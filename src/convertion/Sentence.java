package convertion;

import java.util.ArrayList;
import java.util.Set;

import javax.sound.midi.Soundbank;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.omg.CORBA.StringHolder;

import com.sun.java.swing.plaf.windows.WindowsOptionPaneUI;
import com.sun.org.apache.regexp.internal.recompile;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.StringStack;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import jdk.nashorn.internal.runtime.Undefined;
import jvntextpro.JVnTextPro;
import sun.security.util.Length;

public class Sentence {
	private String text;
	private Entity[] entities;
	private ArrayList<Word> words;
	int max_level;
	private static final String regex = " |\\_";
	
	public Sentence (){
		words = new ArrayList<Word>();
	}
	
	public Sentence(Sentence sen){
		text = sen.text;
		entities = sen.entities == null ? null : sen.entities.clone();
		words = (ArrayList<Word>) sen.words.clone();
		max_level = sen.max_level;
	}
	
	
	public void showSentence(){
		System.out.println("text : " + text);
		for (Entity entity : entities) {
			entity.showEntity();
		}
	}
	
	
	public String WordsFormatted() {
		String wordsFormatted = "";
		for(int i = 0; i < words.size() ; i++){
			wordsFormatted = wordsFormatted + words.get(i).showWord_standar(max_level);
		}
		return wordsFormatted;
	}
	
	
	
	public void setPOS(JVnTextPro textPro){
		String textPro_result = textPro.posTagging(textPro.wordSegment(text));
		String[] result = textPro_result.split(" ");
		System.out.println(textPro_result);
		{
			int index = 0;
			for (String string : result) {
				
//				System.out.println("string in result : " +string);
				String posTag;
				String[] wordsResult;
				if(string.split("/").length == 0 ) {
					posTag="/";
					wordsResult =  new String[]{"/"};
				}
				else
				{
					String[] split = string.split("/");
					posTag = split[split.length-1];
					String word = "";
					for(int i = 0 ; i < split.length-1;i++){
						word += split[i];
					}
					wordsResult = Preprocess.splitWord(word, "_");
				}
//				System.out.println(string.split("/")[0] + "  /  " +posTag);
				
				boolean flag_merger = true;
				int tem = index;
				String levelsToString = words.get(index).bio.toString() ;
//				System.out.println("index"+ levelsToString );
				if(wordsResult.length > 1)
					{
					System.out.println("LEN >1 index ###"+ index + " : " +  words.get(tem).content + "---"+ string);
						if((words.get(tem).content.replace(",", "")).equals(wordsResult[0].replace(",", ""))){
							for (String sub_word : wordsResult)  {
//								System.out.println("get word " + words.get(tem).content + " // subword :  "+sub_word);
//									System.out.println(words.get(tem).bio.toString() + " --- "+ levelsToString);
									if(!(words.get(tem).bio.toString()).equals(levelsToString)) 
									{ 
//										System.out.println("OK");
										flag_merger = false;
										break;
									}
									tem++;
								
							}
							if(flag_merger){
								words.get(index).pos=posTag;
								words.get(index).content =  string.split("/")[0];
								for(int i = index+1; i < (index+wordsResult.length);i++) {
//									System.out.println(words.get(index+1).content);
									words.remove(index+1);
								
								}
								index++;
//								System.out.println(words.toString());
							}
							else{
								int tem2 = index;
								for(int i = tem2; i < (tem2+wordsResult.length);i++) {
//									System.out.println("#NOT-MERGER: #" + i + " lenght " + words.get(i).content);
									words.get(i).pos=posTag;
									index++;
								}
								
							}
							
						}
						else System.out.println("some thing's wrong at ---"+ string + "--- and word ---" + words.get(tem).content + "--- in sentence : " + text  );
						
					}
				else 
					{
//						System.out.println("### " + index + " : single "+ words.get(index).content + "/ " + string);
//					if(wordsResult.length == 0 || wordsResult[0].equals(",") ) continue;
						if(wordsResult.length == 0  ) continue;
						words.get(index).pos=posTag;
						words.get(index).content = wordsResult[0];
						index++;
					}
				
			}
			
		}
//		System.out.println("\nAfter set POS : ");
		
		
	}
	

	
	public void setBIO(){
		int level = 0;
		max_level = -1;
		while(true){
			boolean flag_start = true;
			boolean flag_end_loop = true;
//			String prevLevel = "";
			int prev_entity_id = -1;
			String currentLevel;
			int currentId;
//			System.out.println("LV : "+level);
			
			for (Word word : words) {
//				System.out.println(prevLevel);
//				set maxlevel
				if(max_level < word.bio.size()) max_level = word.bio.size();
				if(level < word.bio.size()) {
					flag_end_loop=false;
					if( word.bio.get(level).equals("O")) {
//						prevLevel = "O";
						continue;
					}
				  currentLevel = word.bio.get(level);
					currentId = word.entity_id.get(level);
//					System.out.println("Currrent level : "+currentLevel + "// " + word.bio.toString());
//					if(!currentLevel.equals(prevLevel)){
//						prevLevel = currentLevel;
//						word.bio.set(level, "B-" + currentLevel);
//					}
//					else {
//						word.bio.set(level, "I-" + currentLevel);
//					}
					if(currentId != prev_entity_id){
						prev_entity_id = currentId;
						word.bio.set(level, "B-" + currentLevel);
					}
					else {
						word.bio.set(level, "I-" + currentLevel);
					}
				}
				else{
//					prevLevel = "0";
					prev_entity_id = -1;
					continue;
				}	
			}
			if(flag_end_loop) break;
			level++;
		}
	}
	
	public void testGetWordsBIO() {
		words = new ArrayList<Word>();
		int start_point = 0;
		int count_id = 0;
		for (Entity entity : entities) {
//			System.out.println("???" + text);
			entity.setValue(text.substring(entity.getStart(),entity.getEnd()));
			if(entity.getStart()!=start_point)
			{
				String subEntity = text.substring(start_point, entity.getStart()).trim();
				subEntity.replace("_", " ");
				for (String w : Preprocess.splitWord(text.substring(start_point, entity.getStart()).trim(), regex)) {
//					if(w.equals("") || w.equals(",")|| w.equals(".")) continue;
					ArrayList<String> baseLevel = new ArrayList<>();
					ArrayList<Integer> entity_id = new ArrayList<>();
					baseLevel.add("O");
					entity_id.add(count_id++);
					words.add(new Word(w, baseLevel, entity_id));
				}
			}

			ArrayList<String> levels = new ArrayList<String>();
			ArrayList<Integer> entity_id = new ArrayList<>();
			getWordsFromEntity(entity, levels,words, entity_id, count_id);
			count_id++;
			start_point=entity.getEnd();
			
			
		}
		if(start_point != text.length()){
			for (String w : Preprocess.splitWord(text.substring(start_point, text.length()).trim(), regex)) {
//				if(w.equals("")|| w.equals(",") || w.equals(".")) continue;
				ArrayList<String> baseLevel = new ArrayList<>();
				ArrayList<Integer> entity_id = new ArrayList<>();
				baseLevel.add("O");
				entity_id.add(count_id++);
				words.add(new Word(w, baseLevel, entity_id));
			}
		}
		setBIO();
		System.out.println("\n\nAfter set BIO notation : ");
		for (Word word : words) {
			word.showWord();
		}
	}
	
	public void setSubEntitiesFromWords(Entity entity,int start_index,int end_index,int level){
		if(level > max_level) return;
		String entityValue = "";
		String entityName = "";
		int startId_word = 0 ;
		int endId_word= 0;
		int startId_character = 0 ;
		int endId_character = 0;
		boolean onChange = false;
		for(int i = start_index; i <= end_index; i++){
			Word word = words.get(i);
			String BIO_flag = word.bio.get(level-1);
//			System.out.println("###" + level + " : "  + BIO_flag + " --- "+ word.content);
			
			if(BIO_flag.equals("O")) {
				if(onChange){
					Entity newEntity = new Entity(entityName, entityValue, startId_character, endId_character);
					entity.addSubEntity(newEntity);
					setSubEntitiesFromWords(newEntity, startId_word, endId_word, level+1);
					onChange = false;
				}
				continue;
			}
			else
			{
				if(BIO_flag.startsWith("B-")){
					if(i == end_index){
						Entity newEntity = new Entity(BIO_flag.split("-")[1],  word.content, startId_character, endId_character);
						entity.addSubEntity(newEntity);
					}
					if(onChange) {
						Entity newEntity = new Entity(entityName, entityValue, startId_character, endId_character);
						entity.addSubEntity(newEntity);
						setSubEntitiesFromWords(newEntity, startId_word, endId_word, level+1);
					}
					onChange = true;
					entityName = BIO_flag.split("-")[1];
					entityValue="";
					startId_word = i;
					endId_word = i;
				}
				else{
					
					endId_word = i ;
					if(i == end_index && onChange){
						entityValue += " "+ word.content;
						Entity newEntity = new Entity(entityName, entityValue, startId_character, endId_character);
						entity.addSubEntity(newEntity);
						setSubEntitiesFromWords(newEntity, startId_word, endId_word, level+1);
					}
				}
				entityValue += " "+ word.content;
			}
			
//				if(entityValue.length()>0){
//					Entity newEntity = new Entity(entityName, entityValue, startId_character, endId_character);
//					entity.addSubEntity(newEntity);
//					setSubEntitiesFromWords(newEntity, startId_word, i-1, level+1);
//					startId_word = i;
//				}
//				entityName = BIO_flag.split("-")[1];
//				entityValue += word.content;
////				to be continue
//				
//			}
//			else
//			{
//				
//			}
		}
		
	}
	
	public void _setEntities() {
		Entity root = new Entity("root", "root", 0, 0);
		setSubEntitiesFromWords(root, 0, words.size()-1, 1);
		
		root.showEntity();
		entities = root.getSubentities();
	}
	
	/**
	 * @purpose thêm từng từ vào wordsList 
	 * @param entity
	 * @param levels
	 * @param wordsList
	 */
	public void getWordsFromEntity(Entity entity,ArrayList<String> levels, ArrayList<Word> wordsList, ArrayList<Integer> entity_id, int count_id){
		int start_point = 0;
		levels.add(entity.getEntity());
		entity_id.add(count_id);
		System.out.println("###" + entity.getValue());
	
		if(entity.getSubentities()!= null) {
			boolean lowLevelFlagStart =true;
			int count_id_sub = 0;
			if(entity.getSubentities()[0].getStart() != 0) lowLevelFlagStart = false;
			for (Entity subEn : entity.getSubentities()) {
//				System.out.println("###" + entity.getValue());
				subEn.setValue(entity.getValue().substring(subEn.getStart(),subEn.getEnd()));
//				String formated_suben_value = 
//				nếu ở giữa có những từ không được gán, thì cho những từ này vào wordlist
				if(subEn.getStart()>start_point){
					
//					System.out.println("??? "+entity.getValue()+ " "+start_point + " --- " + subEn.getStart());
					String getUnprocessText= entity.getValue().substring(start_point, subEn.getStart()).trim();
					System.out.println("between : " + getUnprocessText);
//					for (String word : getUnprocessText.split(" ")) {
//						if(word.equals("")|| word.equals(",") || word.equals(".") ) continue;
					for (String word : Preprocess.splitWord(getUnprocessText, regex)) {
						wordsList.add( new Word(word,(ArrayList<String>) levels.clone(), (ArrayList<Integer>) entity_id.clone()));
					}
				}
				
			
					getWordsFromEntity(subEn, (ArrayList<String>) levels.clone(),wordsList, (ArrayList<Integer>) entity_id.clone(), count_id_sub);
					lowLevelFlagStart = false;
					if(subEn.getEnd() != entity.getValue().length())
					{
						if(entity.getValue().charAt(subEn.getEnd())!=" ".charAt(0)) start_point = subEn.getEnd();
						else start_point = subEn.getEnd()+1;
					}
					else{
						start_point = subEn.getEnd()+1;
					}
						
					count_id_sub++;
			}
//			cho những từ cuối cùng của entity này vào wordlist nếu nó không được gán gì cả
			if(start_point != entity.getValue().length()+1){
				for (String w : Preprocess.splitWord(entity.getValue().substring(start_point, entity.getValue().length()).trim(),regex)) {
//					if(w.equals("")|| w.equals(",") || w.equals(".")) continue;
					words.add(new Word(w, (ArrayList<String>) levels.clone(), (ArrayList<Integer>) entity_id.clone()));
				}
			}
			
		}
		else{
		
//			String[] words = entity.getValue().split(" ");
			System.out.println("LEAF: " + entity.getValue());
			String[] words = Preprocess.splitWord(entity.getValue(), regex);
			
			for (String word : words) {
//				System.out.println("### " +word + " ---> " + levels.toString() );
//				if(word.equals("")|| word.equals(",") || word.equals(".")) continue;
				wordsList.add(new Word(word,(ArrayList<String>) levels.clone(),(ArrayList<Integer>) entity_id.clone()));
				
			}
			return;
		}
	}
	
	// add a word to word list and set max level of word list
	public void setWordsFromString(String str) {
//		words = new ArrayList<Word>();
		Word newWord = new Word(str);
		if(text == null) text = newWord.content;
		else text = text +  " " + newWord.content;
		words.add(newWord);
		if(max_level < newWord.getLevel()) max_level = newWord.getLevel();
	}
	/**
	 * @return the entities
	 */
	public Entity[] getEntities() {
		return entities;
	}
	/**
	 * @param entities the entities to set
	 */
	public void setEntities(Entity[] entities) {
		this.entities = entities;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the words
	 */


}
